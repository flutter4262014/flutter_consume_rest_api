# flutter_consume_rest_api

## Consuming a REST API in Flutter

This small app shows how to use Flutter to create, retrieve, update and delete data inside a REST API.

We are going to learn multiple approaches of interacting with a REST API. We are going to learn how to serialize plain dart objects to json  in a manual way, but also using a library called JSON serializer.

We are also going to learn how to make HTTP requests with the http library, but also how to make HTTP requests with the Chopper library. Our Flutter app will be connected to a back end developped with spring boot.

This application was largely developed by following [these](https://www.youtube.com/watch?v=M8zM48Jytv0&list=PL_Wj0DgxTlJeLFYfRBfpFveEd9cQfIpDx&index=1) tutorials.

## Versions of tools used

- Flutter 3.3.8
- http 0.12.0+2
- chopper 3.0.1