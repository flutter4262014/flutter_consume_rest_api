import 'package:flutter/material.dart';

class NoteAddDialog extends StatelessWidget {
  final String content;

  const NoteAddDialog({Key? key, required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Done'),
      content: Text(content),
      actions: <Widget>[
        TextButton(
          child: Text('Ok'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
