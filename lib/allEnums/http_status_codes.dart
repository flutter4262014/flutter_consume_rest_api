enum HttpStatusCodes {
  OK(200),
  CREATED(201),
  NO_CONTENT(204),
  UNAUTHORIZED(401);

  final int value;

  const HttpStatusCodes(this.value);
}