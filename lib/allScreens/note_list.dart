import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../allDialogs/note_delete_dialog.dart';
import '../allModels/api_response.dart';
import '../allModels/note_for_listing.dart';
import '../allServices/notes_service.dart';
import 'note_modify.dart';

class NoteList extends StatefulWidget {
  @override
  State<NoteList> createState() => _NoteListState();
}

class _NoteListState extends State<NoteList> {
  NotesService get noteService => GetIt.I<NotesService>();
  late ApiResponse<List<NoteForListing>> _apiResponse;
  bool _isLoading = false;

  String formatDateTime(DateTime dateTime) {
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  // Method that gets called at the opening of our stateful page
  @override
  void initState() {
    _fetchNotes();
    super.initState();
  }

  void _fetchNotes() async {
    setState(() {
      _isLoading = true;
    });

    _apiResponse = await noteService.getNotesList();

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('List of Notes')),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => NoteModify(noteId: '')))
                .then((_) {
              _fetchNotes();
            });
          },
          child: Icon(Icons.add),
        ),
        body: Builder(
          builder: (_) {
            if (_isLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (_apiResponse.error) {
              return Center(
                child: Text(_apiResponse.errorMessage),
              );
            }
            return ListView.separated(
              separatorBuilder: (_, __) => Divider(
                height: 1,
                color: Colors.blue,
              ),
              itemBuilder: (_, index) {
                return Dismissible(
                  key: ValueKey(_apiResponse.data![index].noteId),
                  direction: DismissDirection.startToEnd,
                  onDismissed: (direction) {},
                  confirmDismiss: (direction) async {
                    final result = await showDialog(
                        context: context, builder: (_) => NoteDeleteDialog());
                    if (result) {
                      final deleteResult = await noteService
                          .deleteNote(_apiResponse.data![index].noteId);
                      var message = (deleteResult != null &&
                              deleteResult.data == true)
                          ? 'The note was deleted successfully'
                          : deleteResult.errorMessage ?? 'An error occurred';
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(message),
                          duration: Duration(milliseconds: 3000)));
                      return deleteResult.data ?? false;
                    }
                    return result;
                  },
                  background: Container(
                    color: Colors.red,
                    padding: EdgeInsets.only(left: 16),
                    child: Align(
                      child: Icon(
                        Icons.delete,
                        color: Colors.white,
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                  child: ListTile(
                    title: Text(
                      _apiResponse.data![index].noteTitle,
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    subtitle: Text(
                        'Last Edited on ${formatDateTime(_apiResponse.data![index].lastEditDateTime ?? _apiResponse.data![index].createDateTime)}'),
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(
                              builder: (_) => NoteModify(
                                  noteId: _apiResponse.data![index].noteId)))
                          .then((value) {
                        _fetchNotes();
                      });
                    },
                  ),
                );
              },
              itemCount: _apiResponse.data!.length,
            );
          },
        ));
  }
}
