import 'package:flutter/material.dart';
import 'package:flutter_consume_rest_api/allModels/note.dart';
import 'package:flutter_consume_rest_api/allServices/notes_service.dart';
import 'package:get_it/get_it.dart';

import '../allDialogs/note_add_dialog.dart';

class NoteModify extends StatefulWidget {
  final String noteId;

  NoteModify({required this.noteId});

  @override
  State<NoteModify> createState() => _NoteModifyState();
}

class _NoteModifyState extends State<NoteModify> {
  bool get isEditing => widget.noteId != '';

  NotesService get notesService => GetIt.I<NotesService>();

  String? errorMessage;
  late Note note;

  TextEditingController _titleController = TextEditingController();
  TextEditingController _contentController = TextEditingController();

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    if (isEditing) {
      setState(() {
        _isLoading = true;
      });
      notesService.getNote(widget.noteId).then((response) {
        setState(() {
          _isLoading = false;
        });
        if (response.error) {
          errorMessage = response.errorMessage ?? 'An error occurred';
        }
        note = response.data!;
        _titleController.text = note.noteTitle;
        _contentController.text = note.noteContent;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(isEditing ? 'Edit Note' : 'Create Note')),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Column(
                children: <Widget>[
                  TextField(
                    controller: _titleController,
                    decoration: InputDecoration(hintText: 'Note title'),
                  ),
                  Container(height: 8),
                  TextField(
                    controller: _contentController,
                    decoration: InputDecoration(hintText: 'Note content'),
                  ),
                  Container(height: 16),
                  SizedBox(
                    width: double.infinity,
                    height: 35,
                    child: ElevatedButton(
                      onPressed: () async {
                        if (isEditing) {
                          setState(() {
                            _isLoading = true;
                          });
                          final note = Note(
                              noteId: widget.noteId,
                              noteTitle: _titleController.text,
                              noteContent: _contentController.text,
                              createDateTime: DateTime.now(),
                              lastEditDateTime: DateTime.now());
                          final result = await notesService.updateNote(widget.noteId, note);
                          setState(() {
                            _isLoading = false;
                          });
                          showDialog(
                              context: context,
                              builder: (_) => NoteAddDialog(
                                content: result.error
                                    ? (result.errorMessage ??
                                    'An error occurred')
                                    : 'Your note was updated',
                              )).then((data) {
                            if (result.data!) {
                              Navigator.of(context).pop();
                            }
                          });
                        } else {
                          setState(() {
                            _isLoading = true;
                          });
                          final note = Note(
                              noteId: '',
                              noteTitle: _titleController.text,
                              noteContent: _contentController.text,
                              createDateTime: DateTime.now(),
                              lastEditDateTime: DateTime.now());
                          final result = await notesService.createNote(note);
                          setState(() {
                            _isLoading = false;
                          });
                          showDialog(
                              context: context,
                              builder: (_) => NoteAddDialog(
                                    content: result.error
                                        ? (result.errorMessage ??
                                            'An error occurred')
                                        : 'Your note was created',
                                  )).then((data) {
                            if (result.data!) {
                              Navigator.of(context).pop();
                            }
                          });
                        }
                      },
                      child: Text('Submit'),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Theme.of(context).primaryColor,
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }
}
