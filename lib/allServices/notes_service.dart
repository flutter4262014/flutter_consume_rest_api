import 'dart:convert';

import '../allEnums/http_status_codes.dart';
import '../allModels/api_response.dart';
import '../allModels/note.dart';
import '../allModels/note_for_listing.dart';
import 'package:http/http.dart' as http;

class NotesService {
  static const API = 'http://10.0.2.2:8080/api/flutter/rest';
  static const headers = {'Content-Type': 'application/json'};

  Future<ApiResponse<List<NoteForListing>>> getNotesList() {
    return http.get(Uri.parse(API + '/notes')).then((data) {
      if (data.statusCode == HttpStatusCodes.OK.value) {
        final jsonData = json.decode(data.body);
        final notes = <NoteForListing>[];
        for (var item in jsonData) {
          notes.add(NoteForListing.fromJson(item));
        }
        return ApiResponse<List<NoteForListing>>(data: notes, errorMessage: '');
      }
      return ApiResponse<List<NoteForListing>>(
          data: [], error: true, errorMessage: 'An error occurred');
    }).catchError((_) => ApiResponse<List<NoteForListing>>(
        data: [], error: true, errorMessage: 'An error occurred'));
  }

  Future<ApiResponse<Note>> getNote(String noteId) {
    return http.get(Uri.parse(API + '/find/' + noteId)).then((data) {
      if (data.statusCode == HttpStatusCodes.OK.value) {
        final jsonData = json.decode(data.body);
        print(jsonData);
        return ApiResponse<Note>(
            data: Note.fromJson(jsonData), errorMessage: '');
      }
      return ApiResponse<Note>(
          data: null, error: true, errorMessage: 'An error occurred');
    }).catchError((_) => ApiResponse<Note>(
        data: null, error: true, errorMessage: 'An error occurred'));
  }

  Future<ApiResponse<bool>> createNote(Note note) {
    return http
        .post(Uri.parse(API + '/add'),
            body: json.encode(note.toJson()), headers: headers)
        .then((data) {
      if (data.statusCode == HttpStatusCodes.OK.value) {
        return ApiResponse<bool>(data: true, errorMessage: '');
      }
      return ApiResponse<bool>(
          data: null, error: true, errorMessage: 'An error occurred');
    }).catchError((_) => ApiResponse<bool>(
            data: null, error: true, errorMessage: 'An error occurred'));
  }

  Future<ApiResponse<bool>> updateNote(String noteId, Note note) {
    return http
        .put(Uri.parse(API + '/update/' + noteId),
            body: json.encode(note.toJson()), headers: headers)
        .then((data) {
      if (data.statusCode == HttpStatusCodes.OK.value) {
        return ApiResponse<bool>(data: true, errorMessage: '');
      }
      return ApiResponse<bool>(
          data: null, error: true, errorMessage: 'An error occurred');
    }).catchError((_) => ApiResponse<bool>(
            data: null, error: true, errorMessage: 'An error occurred'));
  }

  Future<ApiResponse<bool>> deleteNote(String noteId) {
    return http
        .delete(Uri.parse(API + '/delete/' + noteId), headers: headers)
        .then((data) {
      if (data.statusCode == HttpStatusCodes.NO_CONTENT.value) {
        return ApiResponse<bool>(data: true, errorMessage: '');
      }
      return ApiResponse<bool>(
          data: null, error: true, errorMessage: 'An error occurred');
    }).catchError((_) => ApiResponse<bool>(
            data: null, error: true, errorMessage: 'An error occurred'));
  }
}