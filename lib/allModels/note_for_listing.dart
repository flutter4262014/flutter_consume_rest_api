class NoteForListing {
  String noteId;
  String noteTitle;
  DateTime createDateTime;
  DateTime lastEditDateTime;

  NoteForListing(
      {required this.noteId,
      required this.noteTitle,
      required this.createDateTime,
      required this.lastEditDateTime});

  factory NoteForListing.fromJson(Map<String, dynamic> item) {
    return NoteForListing(
      noteId: item['noteId'],
      noteTitle: item['noteTitle'],
      createDateTime: DateTime.parse(item['createDateTime']),
      lastEditDateTime: DateTime.parse(item['lastEditDateTime']),
    );
  }

  Map<String, dynamic>? toJson() {
    return {
      'noteId': noteId,
      'noteTitle': noteTitle,
      'createDateTime': createDateTime,
      'lastEditDateTime': lastEditDateTime,
    };
  }
}
