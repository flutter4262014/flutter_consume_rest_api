class Note {
  String noteId;
  String noteTitle;
  String noteContent;
  DateTime createDateTime;
  DateTime lastEditDateTime;

  Note(
      {required this.noteId,
      required this.noteTitle,
      required this.noteContent,
      required this.createDateTime,
      required this.lastEditDateTime});

  factory Note.fromJson(Map<String, dynamic> item) {
    return Note(
      noteId: item['noteId'],
      noteTitle: item['noteTitle'],
      noteContent: item['noteContent'],
      createDateTime: DateTime.parse(item['createDateTime']),
      lastEditDateTime: DateTime.parse(item['lastEditDateTime']),
    );
  }

  Map<String, dynamic>? toJson() {
    return {
      'noteId': noteId,
      'noteTitle': noteTitle,
      'noteContent': noteContent,
      'createDateTime': createDateTime.toIso8601String(),
      'lastEditDateTime': lastEditDateTime.toIso8601String(),
    };
  }
}
